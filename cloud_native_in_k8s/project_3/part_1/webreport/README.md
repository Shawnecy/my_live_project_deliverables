A fully bootified, modernized, and converted to a backend service that communicate through a RESTful JSON-based API.

Derived from: https://github.com/grafpoo/restapi-starter

This project is a solution to the second part of the second project, "Convert an App to RESTful API", of the Manning LiveProject: "Converting Legacy Applications to Cloud Native in Kubernetes"

This project has been extended to also satisfy the deliverables for the first part of the third project, "Containerize with Docker", of the same course.

LiveProject course home page: https://www.manning.com/liveprojectseries/converting-to-cloud-native-in-kubernetes

---

Build the custom postgresql Dockerfile:

`docker build -t shawnecy/mypgsql .`

To start the container:

`docker run -p 5432:5432 shawnecy/mypgsql`

To stop the container:

`docker ps -q --filter ancestor=shawnecy/mypgsql | xargs docker stop`

To tag an image and set repo name:

`docker tag shawnecy/mypgsql:latest shawnecy/mypgsql:latest`

To push the image (once the repo exists on docker hub):

`docker push shawnecy/mypgsql:latest`

---

Build the webreport app Dockerfile:

`docker build -t shawnecy/webreport .`

To start the container:

`docker run -p 8080:8080 shawnecy/webreport --server.port=8080`

To stop the container:

`docker ps -q --filter ancestor=shawnecy/webreport | xargs docker stop`

To tag an image and set repo name:

`docker tag shawnecy/webreport:latest shawnecy/webreport:latest`

To push the image (once the repo exists on docker hub):

`docker push shawnecy/webreport:latest`

---

