package liveproject.webreport.match;

import liveproject.webreport.Application;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude = SecurityAutoConfiguration.class)
@AutoConfigureTestDatabase
public class MatchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MatchRepository repository;

    @AfterEach
    public void resetDb() {
        repository.deleteAll();
    }

    private static final String EPL2015 = "epl2015";
    private static final String EPL2015_JSON = EPL2015 + ".json";
    private static final String EPL1979 = "epl1979";

    @Test
    public void sunnyDayScenarioShould20xWithProperJsonResponses() throws Exception {
        final String epl2015Json = loadSeasonJson(EPL2015_JSON);

        mockMvc.perform(post("/match/{season}", EPL2015)
                        .content(epl2015Json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        mockMvc.perform(get("/season", EPL2015)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]", equalTo(EPL2015)))
                .andReturn();

        mockMvc.perform(get("/season-report/{season}", EPL2015)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.homeWins", equalTo(187)))
                .andExpect(jsonPath("$.awayWins", equalTo(109)))
                .andExpect(jsonPath("$.draws", equalTo(84)))
                .andReturn();

        mockMvc.perform(get("/matches-report/{season}", EPL2015)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(380)))
                .andReturn();
    }

    @Test
    public void requestSeasonReportForMissingSeasonShould404() throws Exception {
        mockMvc.perform(get("/season-report/{season}", EPL1979)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void requestMatchesReportForMissingSeasonShould404() throws Exception {
        mockMvc.perform(get("/matches-report/{season}", EPL1979)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    private String loadSeasonJson(final String seasonJsonFileName) throws IOException {
        final ClassLoader classLoader = getClass().getClassLoader();
        try (final InputStream inputStream = classLoader.getResourceAsStream(seasonJsonFileName)) {
            return new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        }
    }
}