CREATE USER mycustomsuperadmin WITH PASSWORD 'changethispassword';
GRANT ALL PRIVILEGES ON DATABASE postgres TO mycustomsuperadmin;
-- Note: Default syntax for creating database in PostgreSQL is “database_name”.”scheme_name”.”table_name”.