package liveproject.webreport.season;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Season {
    private List<TeamResult> teamResults;
    private Integer homeWins;
    private Integer awayWins;
    private Integer draws;
    private Integer goallessDraws;
    private String season;
    private Collection<RefereeResults> refereeResults;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TeamResult {
        private String teamName;
        private Integer played;
        private Integer points;
        private Integer goalDiff;
    }

    public static class SortByPointsAndGoalDiff implements Comparator<TeamResult> {
        // descending sort (so less-than is greater-than
        public int compare(final TeamResult a, final TeamResult b) {
            if (a == null) return 1;
            if (b == null) return 1;
            if (a.points < b.points) return 1;
            if (a.points > b.points) return -1;
            return b.goalDiff.compareTo(a.goalDiff);
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RefereeResults {
        String name = "Nobody";
        Integer homeWins = 0;
        Integer awayWins = 0;
        Integer draws = 0;
        Integer goallessDraws = 0;

        public RefereeResults(final String referee) {
            this.name = referee;
        }

        @Override
        public String toString() {
            return String.format("Referee: %s %s/%s/%s/%s",name,homeWins,awayWins,draws,goallessDraws);
        }
    }
}