package liveproject.webreport.match;

import liveproject.webreport.season.Season;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
public class MatchController {
    private final MatchService matchService;

    @Autowired
    public MatchController(final MatchService matchService) {
        this.matchService = matchService;
    }

    @GetMapping("season")
    @ResponseStatus(value = HttpStatus.OK)
    public List<String> season() {
        return matchService.getAllSeasons();
    }

    @GetMapping("season-report/{season}")
    @ResponseStatus(value = HttpStatus.OK)
    public Season seasonReport(@PathVariable("season") final String season) {
        return matchService.aggregateSeason(season);
    }

    @GetMapping("matches-report/{season}")
    @ResponseStatus(value = HttpStatus.OK)
    public Set<Match> matchesReport(@PathVariable("season") final String season) {
        return matchService.getAllBySeasonSorted(season);
    }

    @PostMapping("match/{season}")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void match(@PathVariable("season") final String season, @RequestBody final List<Match> matches) {
        final Map<String, Integer> counts = matchService.saveAll(season, matches);
    }
}