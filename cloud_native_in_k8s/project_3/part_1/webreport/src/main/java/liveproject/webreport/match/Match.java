package liveproject.webreport.match;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Match implements Comparable {
    @Id
    @GeneratedValue
    private Long id;

    private String division;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yy") private Date gameDate;
    private String gameTime;
    private String homeTeam;
    private String awayTeam;
    private Integer fullTimeHomeGoals;
    private Integer fullTimeAwayGoals;
    private Character fullTimeResult;
    private Integer halfTimeHomeGoals;
    private Integer halfTimeAwayGoals;
    private Character halfTimeResult;
    private String referee;
    private Integer homeTeamShots;
    private Integer awayTeamShots;
    private Integer homeTeamShotsOnTarget;
    private Integer awayTeamShotsOnTarget;
    private Integer homeTeamFouls;
    private Integer awayTeamFouls;
    private Integer homeTeamCorners;
    private Integer awayTeamCorners;
    private Integer homeTeamYellowCards;
    private Integer awayTeamYellowCards;
    private Integer homeTeamRedCards;
    private Integer awayTeamRedCards;
    private String season;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        final Match match = (Match) o;
        return id != null && Objects.equals(id, match.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public int compareTo(final Object o) {
        if (! (o instanceof final Match other)) return 1;
        if (gameDate == null) {
            // if mine is null and other isn't, i come after
            if (other.gameDate != null) return 1;
        } else {
            if (other.gameDate == null) return -1;
            final int dateCompare = gameDate.compareTo(other.gameDate);
            if (dateCompare != 0) return dateCompare;
        }
        if (gameTime == null) {
            // if mine is null and other isn't, i come after
            if (other.gameTime != null) return 1;
        } else {
            if (other.gameTime == null) return -1;
            final int timeCompare = gameTime.compareTo(other.gameTime);
            if (timeCompare != 0) return timeCompare;
        }
        return homeTeam.compareTo(other.homeTeam);
    }
}