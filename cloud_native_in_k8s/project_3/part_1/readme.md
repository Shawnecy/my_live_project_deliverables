The custom postgresql image can be found at:
https://hub.docker.com/repository/docker/shawnecy/mypgsql

Its Dockerfile can be found in the webreport project in this repo at:

`cloud_native_in_k8s/project_3/part_1/webreport/src/main/resources/postgresql/Dockerfile`

---

The image for the java app, webreport, can be found at:
https://hub.docker.com/repository/docker/shawnecy/webreport

Its Docker file cna be found in the webreport project in this repo at:

`cloud_native_in_k8s/project_3/part_1/webreport/Dockerfile`
