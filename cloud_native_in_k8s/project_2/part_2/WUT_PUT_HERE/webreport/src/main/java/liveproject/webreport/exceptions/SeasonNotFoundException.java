package liveproject.webreport.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "season not found")
public class SeasonNotFoundException extends RuntimeException {
}