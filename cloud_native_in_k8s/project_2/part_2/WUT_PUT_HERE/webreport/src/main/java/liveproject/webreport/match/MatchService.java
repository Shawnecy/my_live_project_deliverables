package liveproject.webreport.match;

import liveproject.webreport.exceptions.SeasonNotFoundException;
import liveproject.webreport.season.Season;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

@Log
@Service
public class MatchService {
    private final MatchRepository matchRepository;

    @Autowired
    public MatchService(final MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    @Transactional
    public Match save(final Match match) {
        matchRepository.save(match);
        return match;
    }

    @Transactional
    public Map<String, Integer> saveAll(final String seasonStr, final List<Match> matches) {
        final Map<String, Integer> resultsBySeason = new HashMap<>();
        for (final Match m : matches) {
            final String season = !StringUtils.hasLength(m.getSeason()) ? seasonStr : m.getSeason();
            matchRepository.save(m);
            if (!resultsBySeason.containsKey(season)) {
                resultsBySeason.put(season, 0);
            }
            resultsBySeason.put(season, 1 + resultsBySeason.get(season));
        }
        return resultsBySeason;
    }

    public Set<Match> getAllBySeasonSorted(final String seasonStr) {
		final List<Match> matches = matchRepository.findBySeason(seasonStr);
		if (matches.isEmpty()) {
			throw new SeasonNotFoundException();
		} else {
			return new TreeSet<>(matchRepository.findBySeason(seasonStr));
		}
    }

    public Season aggregateSeason(final String seasonStr) {
        int homeWins = 0;
        int awayWins = 0;
        int draws = 0;
        int goallessDraws = 0;
        final Map<String, Season.RefereeResults> refereeMap = new HashMap<>();
        final Map<String, Integer> teamPlayed = new HashMap<>();
        final Map<String, Integer> teamPoints = new HashMap<>();
        final Map<String, Integer> teamGoalDiff = new HashMap<>();
        final List<Match> matches = matchRepository.findBySeason(seasonStr);

		if (matches.isEmpty()) {
			throw new SeasonNotFoundException();
		}

        log.fine("Match count = " + matches.size());
        for (final Match match : matches) {
            addResults(teamPlayed, teamPoints, teamGoalDiff, match.getAwayTeam(), match.getFullTimeAwayGoals() - match.getFullTimeHomeGoals());
            addResults(teamPlayed, teamPoints, teamGoalDiff, match.getHomeTeam(), match.getFullTimeHomeGoals() - match.getFullTimeAwayGoals());
            final Integer[] addToReferee = new Integer[]{0, 0, 0, 0};
            if (match.getFullTimeHomeGoals() > match.getFullTimeAwayGoals()) {
                homeWins++;
                addToReferee[0]++;
            }
            if (match.getFullTimeHomeGoals() < match.getFullTimeAwayGoals()) {
                awayWins++;
                addToReferee[1]++;
            }
            if (match.getFullTimeHomeGoals() == match.getFullTimeAwayGoals()) {
                draws++;
                addToReferee[2]++;
                if (match.getFullTimeHomeGoals() == 0) {
                    goallessDraws++;
                    addToReferee[3]++;
                }
            }
            adjustRefereeStatistics(refereeMap, match.getReferee(), addToReferee);
        }
        // figure out results. this is a pain because ties on point go to goal difference
        final List<Season.TeamResult> teamResults = new ArrayList<>();
        for (final Map.Entry<String, Integer> team : teamPlayed.entrySet()) {
            final String teamName = team.getKey();
            final Integer played = team.getValue();
            final Integer points = teamPoints.getOrDefault(teamName, -1);
            final Integer goalDiff = teamGoalDiff.getOrDefault(teamName, -1);
            teamResults.add(new Season.TeamResult(teamName, played, points, goalDiff));
        }
        teamResults.sort(new Season.SortByPointsAndGoalDiff());
        log.fine(String.format("EPL played : %s", matchRepository.findAll().size()));

        // stick stuff in map
        final Season season = new Season();
        season.setSeason(seasonStr);
        season.setTeamResults(teamResults);
        season.setHomeWins(homeWins);
        season.setAwayWins(awayWins);
        season.setDraws(draws);
        season.setGoallessDraws(goallessDraws);
        season.setRefereeResults(refereeMap.values());
        return season;
    }

    public List<String> getAllSeasons() {
        return matchRepository.findSeasons();
    }

    private static void adjustRefereeStatistics(final Map<String, Season.RefereeResults> refereeMap, final String referee, final Integer[] newStats) {
        // verify entry for referee exists
        if (!refereeMap.containsKey(referee)) {
            refereeMap.put(referee, new Season.RefereeResults(referee));
        }
        final Season.RefereeResults stats = refereeMap.get(referee);
        stats.setHomeWins(stats.getHomeWins() + newStats[0]);
        stats.setAwayWins(stats.getAwayWins() + newStats[1]);
        stats.setDraws(stats.getDraws() + newStats[2]);
        stats.setGoallessDraws(stats.getGoallessDraws() + newStats[3]);
    }

    private static void addResults(final Map<String, Integer> played, final Map<String, Integer> points, final Map<String, Integer> goalDiff, final String team, final int diff) {
        // verify team exists in maps
        if (!played.containsKey(team)) {
            played.put(team, 0);
        }
        if (!points.containsKey(team)) {
            points.put(team, 0);
        }
        if (!goalDiff.containsKey(team)) {
            goalDiff.put(team, 0);
        }
        played.put(team, 1 + played.get(team));
        if (diff < 0) {
            // no points for you
        } else if (diff > 0) {
            // a win
            points.put(team, 3 + points.get(team));
        } else {
            // a draw
            points.put(team, 1 + points.get(team));
        }
        goalDiff.put(team, diff + goalDiff.get(team));
    }
}